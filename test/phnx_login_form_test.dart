import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:phnx_login_form/phnx_login_form.dart';

void main() {
  group('LoginForm tests', () {
    testWidgets(
      "LoginForm shows the email label",
      (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: Center(
                child: PhnxLoginForm(
                  emailController: TextEditingController(),
                  passwordController: TextEditingController(),
                  onTogglePasswordVisibility: () {},
                  obscureTextOnPassword: true,
                ),
              ),
            ),
          ),
        );

        final emailStringFinder = find.text('e-mail');
        expect(emailStringFinder, findsOneWidget);
      },
    );
    testWidgets(
      "LoginForm shows password label'",
      (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: Center(
                child: PhnxLoginForm(
                  emailController: TextEditingController(),
                  passwordController: TextEditingController(),
                  onTogglePasswordVisibility: () {},
                  obscureTextOnPassword: true,
                ),
              ),
            ),
          ),
        );

        final emailStringFinder = find.text('senha');
        expect(emailStringFinder, findsOneWidget);
      },
    );

    testWidgets(
      "LoginForm shows the forgot my password label",
      (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: Center(
                child: PhnxLoginForm(
                  emailController: TextEditingController(),
                  passwordController: TextEditingController(),
                  onTogglePasswordVisibility: () {},
                  obscureTextOnPassword: true,
                ),
              ),
            ),
          ),
        );

        final emailStringFinder = find.text('Esqueci minha senha');
        expect(emailStringFinder, findsOneWidget);
      },
    );

    testWidgets(
      "LoginForm shows the login action label",
      (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: Center(
                child: PhnxLoginForm(
                  emailController: TextEditingController(),
                  passwordController: TextEditingController(),
                  onTogglePasswordVisibility: () {},
                  obscureTextOnPassword: true,
                ),
              ),
            ),
          ),
        );

        final emailStringFinder = find.text('LOGIN');
        expect(emailStringFinder, findsOneWidget);
      },
    );

    testWidgets(
      "If obscureTextOnPassword is true, shows the expected icon",
      (WidgetTester tester) async {
        await tester.pumpWidget(
          MaterialApp(
            home: Scaffold(
              body: Center(
                child: PhnxLoginForm(
                  emailController: TextEditingController(),
                  passwordController: TextEditingController(),
                  onTogglePasswordVisibility: () {},
                  obscureTextOnPassword: true,
                ),
              ),
            ),
          ),
        );

        final iconFinder = find.byIcon(Icons.visibility);
        expect(iconFinder, findsOneWidget);
      },
    );

    testWidgets(
      "suffixIcon method returns the expected IconButton{Icons.visibility_off} if obscureTextOnPassword is false",
      (WidgetTester tester) async {
        final loginForm = PhnxLoginForm(
          emailController: TextEditingController(),
          passwordController: TextEditingController(),
          onTogglePasswordVisibility: () {},
          obscureTextOnPassword: false,
        );

        final iconButton = loginForm.suffixIcon();

        expect((iconButton.icon as Icon).icon == Icons.visibility_off, true);
      },
    );
  });
}
