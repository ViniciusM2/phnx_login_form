library phnx_login_form;

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PhnxLoginForm extends StatelessWidget {
  const PhnxLoginForm({
    this.isLoading = false,
    this.onSubmit,
    required this.emailController,
    required this.passwordController,
    required this.onTogglePasswordVisibility,
    this.obscureTextOnPassword = true,
  });

  final bool isLoading;
  final VoidCallback? onSubmit;
  final VoidCallback onTogglePasswordVisibility;

  final TextEditingController emailController;
  final TextEditingController passwordController;

  final bool obscureTextOnPassword;

  bool get shouldEnableButtons => !isLoading;
  bool get shouldEnableTextFields => !isLoading;

  IconButton suffixIcon() {
    if (obscureTextOnPassword) {
      return IconButton(
        icon: Icon(Icons.visibility),
        onPressed: onTogglePasswordVisibility,
      );
    } else {
      return IconButton(
        icon: Icon(Icons.visibility_off),
        onPressed: onTogglePasswordVisibility,
      );
    }
  }

  // obscureTextOnPassword
  //     ? IconButton(
  //         onPressed: onTogglePasswordVisibility,
  //         icon: Icon(Icons.visibility),
  //       )
  //     : IconButton(
  //         onPressed: onTogglePasswordVisibility,
  //         icon: Icon(Icons.visibility_off),
  //       );

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SizedBox(
          width: 300,
          child: Column(
            children: [
              TextFormField(
                onFieldSubmitted: (String text) {
                  if (shouldEnableButtons) {
                    onSubmit!();
                  }
                },
                onEditingComplete: shouldEnableButtons ? onSubmit : () {},
                decoration: const InputDecoration(
                  labelText: 'e-mail',
                ),
                enabled: shouldEnableTextFields,
                controller: emailController,
              ),
              const SizedBox(
                height: 8,
              ),
              TextFormField(
                onFieldSubmitted: (String text) {
                  if (shouldEnableButtons) {
                    onSubmit!();
                  }
                },
                onEditingComplete: shouldEnableButtons ? onSubmit : () {},
                decoration: InputDecoration(
                  labelText: 'senha',
                  suffixIcon: suffixIcon(),
                ),
                enabled: shouldEnableTextFields,
                obscureText: obscureTextOnPassword,
                controller: passwordController,
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      onPressed: shouldEnableButtons ? onSubmit : null,
                      child: const Padding(
                        padding: EdgeInsets.symmetric(vertical: 16),
                        child: Text('LOGIN'),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(
                    child: MouseRegion(
                      cursor: SystemMouseCursors.click,
                      child: GestureDetector(
                        // onTap: () {},
                        child: Text(
                          'Esqueci minha senha',
                          textAlign: TextAlign.end,
                          style: Get.theme.textTheme.subtitle1!.apply(
                            decoration: TextDecoration.underline,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
